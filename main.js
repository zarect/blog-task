// variables
let modal = document.querySelector('.modal');
let addPostButton = document.querySelector('.new-post__add-post');
let editPostButton = document.querySelectorAll('.blog-listing__btn-edit');
let closeModal = document.querySelectorAll('.modal__close-button');


// open and close modal events on post
editPostButton.forEach(item => {
    item.addEventListener('click', event => {
        modal.style.display = 'block'
    })
});

closeModal.forEach(item => {
    item.addEventListener('click', event => {
        modal.style.display = 'none'
    })
})


